import threading
import socket
import getopt
import psutil
import sys
import re
from queue import Queue


ipr = re.compile(r"^\d+\.\d+\.\d+\.\d+$")
drex = re.compile(r"(?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?\.)+[a-z0-9]"
                  r"[a-z0-9-]{0,61}[a-z0-9]")


def is_valid_ip(ip_address: str) -> bool:
    try:
        socket.inet_aton(ip_address)
        return True
    except socket.error:
        return False


def valid_target(target: str) -> bool:
    is_valid = False
    if ipr.search(target):
        if is_valid_ip(target):
            is_valid = True
    elif drex.search(target):
        is_valid = True

    return is_valid


def pscan(target: str, port: int, timeout: int = 1):
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.settimeout(timeout)
        s.connect((target, port))
        s.close()
        print(f"[*] Port {port} is open on {target}")
    except socket.timeout:
        print(f"[*] Port {port} is closed on {target}")
        s.close()
    except socket.error:
        print(f"[*] An error occured while scanning {port} on target {target}")


class PscanThread(threading.Thread):
    """ Thread class to execute scan for each port on target host """
    def __init__(self, q: Queue):
        threading.Thread.__init__(self)
        self.queue = q

    def run(self):
        while True:
            target, port, timeout = self.queue.get()
            pscan(target=target, port=port, timeout=timeout)
            self.queue.task_done()


def main(argv):
    long_opts = ["help", "target=", "range=", "list=", "timeout=", "threaded="]
    help_msg = """
/*=-- Python Network Port Scanner --=*/
Example Usage: ppscanner --target braintrace.com --range 1-1023
    -t, --target        The target Hostname or IP
    -r, --range         A specific port range
    -l, --list          A comma seperated list of ports
    -s, --timeout       Socket timeout in seconds, default is 1
    -m, --threaded      Using multi-threading to perform the port scan
    -h, --help          Displays this message
    """
    target = str()
    start = int()
    end = int()
    port_list = list()
    timeout = 1
    threaded = False

    try:
        opts, args = getopt.getopt(argv, "ht:r:l:s:m", long_opts)
    except getopt.GetoptError as err:
        print(err)
        print(help_msg)
        sys.exit(2)

    for o, a in opts:
        if o in ("-h", "--help"):
            print(help_msg)
            sys.exit(0)
        elif o in ("-t", "--target"):
            target = a
        elif o in ("-r", "--range"):
            if "-" in a:
                r = a.split("-")
                start = int(r[0])
                end = int(r[1])
            else:
                print("Please use a valid port range!")
                sys.exit(1)
        elif o in ("-l", "--list"):
            port_list = [int(p) for p in a.split(",")]
        elif o in ("-s", "--timeout"):
            timeout = int(a)
        elif o in ("-m", "--threaded"):
            threaded = True

    if not valid_target(target):
        print("You must provide a valid hostname or valid IP address!")
        sys.exit(1)

    if not port_list:
        port_list = [i for i in range(start, end)]

    if not threaded:
        for p in port_list:
            pscan(target=target, port=p, timeout=timeout)
    else:
        cpuc = psutil.cpu_count(logical=True)
        qlock = threading.Lock()
        q = Queue(cpuc)

        scan_thread = PscanThread(q)
        scan_thread.daemon = True
        scan_thread.start()

        qlock.acquire()
        things = [(target, p, timeout) for p in port_list]
        for thing in things:
            q.put(thing)
        qlock.release()
        q.join()


if __name__ == '__main__':
    main(sys.argv[1:])
    sys.exit(0)
